package vn.com.kma.hatuan314.loginmvvm.viewmodels;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import vn.com.kma.hatuan314.loginmvvm.BR;
import vn.com.kma.hatuan314.loginmvvm.models.Login;

public class LoginViewModel extends BaseObservable {
    private Login login;

    public LoginViewModel() {
        this.login = new Login("", "");
    }

    @Bindable
    public String getUsername() {
        return login.getUsername();
    }

    @Bindable
    public void setUsername(String username) {
        login.setUsername(username);
        notifyPropertyChanged(BR.username);
    }

    @Bindable
    public String getPassword() {
        return login.getPassword();
    }

    @Bindable
    public void setPassword(String password) {
        login.setPassword(password);
        notifyPropertyChanged(BR.password);
    }

    public void btnLoginOnClick() {

    }
}
