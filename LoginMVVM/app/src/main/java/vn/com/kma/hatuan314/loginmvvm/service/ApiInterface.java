package vn.com.kma.hatuan314.loginmvvm.service;

import android.util.Log;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import vn.com.kma.hatuan314.loginmvvm.models.Login;

interface ApiInterface {

    @POST("/api/login")
    Call<Login> createUser(@Body Login login);
}