package vn.com.kma.hatuan314.loginmvvm.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import vn.com.kma.hatuan314.loginmvvm.R;
import vn.com.kma.hatuan314.loginmvvm.databinding.ActivityMainBinding;
import vn.com.kma.hatuan314.loginmvvm.viewmodels.LoginViewModel;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activityMainBinding.setViewModel(new LoginViewModel());

        activityMainBinding.executePendingBindings();
    }
}
